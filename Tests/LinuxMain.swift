import XCTest

import spmDemoTests

var tests = [XCTestCaseEntry]()
tests += spmDemoTests.allTests()
XCTMain(tests)
