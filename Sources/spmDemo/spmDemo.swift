struct spmDemo {
    var text = "Hello, World!"
}

public struct Calculator {
    
    public init() {
        
    }
    
    public func add(num1: Int, num2: Int) -> Int {
        return num1 + num2
    }
}
